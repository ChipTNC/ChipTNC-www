#!/usr/bin/python

import web
import datetime
import csv
import os
import aprs
import kiss
import time
import sqlite3
import logging
import subprocess
from gps3 import gps3
from web import form
from map import map
from message import message, messages
from urllib import quote
from settings import settings
from settings import gpios
from config import *

logger = logging.getLogger( "ChipTNC" )
hdlr = logging.FileHandler('/tmp/chiptnc.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel( logging.DEBUG )

render = web.template.render('templates/')

urls = (
    '/', 'index',
    '/logs', 'logs',
    '/settings', 'settings',
    '/recent', 'recent',
    '/message', 'message',
    '/messages', 'messages',
    '/gpios', 'gpios',
    '/map$', 'map',
    '/map/(.+)/(.+)/(.+)', 'map',
)

def tail(f, n):
    assert n >= 0
    pos, lines = n+1, []
    while len(lines) <= n:
        try:
            f.seek(-pos, 2)
        except IOError:
            f.seek(0)
            break
        finally:
            lines = list(f)
            pos *= 2

    return lines[-n:]

class index:
    def GET(self):
        header = ''
        text = ""
        #bus = SMBus(0) 
        #bus.write_byte_data( 0x34, 0x82, 0xff ) 
        #b = bus.read_byte_data( 0x34, 0 )
        try :
            gps_socket = gps3.GPSDSocket()
            data_stream = gps3.DataStream()
            gps_socket.connect()
            gps_socket.watch()
        except :
            if gps_socket :
                gps_socket.close()
            gps_socket = None
        #os.system( "/usr/sbin/i2cset -y -f 0 0x34 0x82 0xff" )
        s = subprocess.check_output( [ "/usr/sbin/i2cget", "-y",  "-f",  "0", "0x34", "0x00" ]  )
        b = int( s, 16 )
        STATUS_ACIN=( b & 0x80 ) >> 7
        STATUS_ACIN_AVAIL=( b & 0x40 ) >> 6 
        STATUS_VBUS=( b & 0x20 ) >> 5
        STATUS_VBUS_AVAIL=( b & 0x10 ) >> 4
        STATUS_VHOLD=( b & 0x08 ) >> 3 
        STATUS_CHG_DIR=( b & 0x04 ) >> 2
        ACVB_SHORT=( b & 0x02 ) >> 1
        STATUS_BOOT=( b & 0x01 )
        
        text += '<div class="row">\n'
        if STATUS_CHG_DIR == 1 :
            text += "<div class='col-sm-6'>Battery Charging</div>"
        else :
            text += "<div class='col-sm-6'>Battery Dis-charging</div>"
            
        text += "</div>"
            
        
        s = subprocess.check_output( [ "/usr/sbin/i2cget", "-y",  "-f",  "0", "0x34", "0xb9" ]  )
        b = int( s, 16 )
        #b = bus.read_byte_data( 0x34, 0x89) 
        text += '<div class="row">\n'
        text += "<div class='col-sm-12'>Battery percent %s</div>" % b
        text += "</div>"

        lat = '0'
        lon = '0'

        gps_online = False

        try :
            if gps_socket :
                for i in range( 0,10 ) :
                    time.sleep( 0.1 )
                    new_data = gps_socket.next()
                    if new_data :
                        data_stream.unpack(new_data)
                        if data_stream.TPV['lat'] and data_stream.TPV['lat'] != 'n/a':
                            lat = str( data_stream.TPV['lat'] )
                        if data_stream.TPV['lon'] and data_stream.TPV['lon'] != 'n/a':
                            lon = str( data_stream.TPV['lon'] )
                    if lon != '0' and lat != '0' :
                        gps_online = True
                        break
                gps_socket.close()
            else :
                gps_online = False
        except :
            gps_online = False
            pass

        if gps_online :
            text += "<div class='row'>"
            text += "<div class='col-sm-3'>"
            text += "lat " + lat 
            text += "</div>\n"
            text += "<div class='col-sm-3'>"
            text += "lon " + lon
            text += "</div>\n"
            text += "</div>\n"
        else :
            text += "<div class='row'>"
            text += "<div class='col-sm-6'>"
            text += "GPS Offline"
            text += "</div>\n"
            text += "</div>\n"
            
        return render.base(header,text)

class test:
    def GET(self):
        header = ''
        text = "WiFi to APRS gateway"
        return render.test()

class logs:
    def GET(self):
        header = ''
        text = ''
        style = 'style="background-color:lavenderblush;"'
        try :
            filename = "/var/log/supervisor/direwolf-stdout.log"
            log = open( filename, 'rb')

            lines = tail( log, 100 )
        except :
            lines = ''

        for line in lines :
            text += '<div class="row">\n'
            text += '<div class="col-sm-12" %s>%s</div>\n' % ( style, unicode( line, errors='replace'))
            text += "</div>"

        return render.base(header,text)

class recent:
    def GET(self):
        header = '<meta http-equiv="refresh" content="30">'
        text = ''
        rows = []
        today = datetime.date.today()

        try :
            filename = "/var/log/direwolf/%d-%02d-%02d.log" % ( today.year, today.month, today.day )
            style = 'style="background-color:%s;"' % listColour0
            csvfile = open( filename, 'rb')
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            csvHeader = reader.next()
            #text += "<div class='row'>" 
            #i = 0
            #for h in csvHeader :
            #    text += "%d:%s " % ( i, h )
            #    i += 1
            #text += '<div>'
            while True :
                try :
                    row = reader.next()
                except StopIteration :
                    break
                except :
                    print( "csv row falied " + ' '.join( row ) )
                rows.append( row )

            rows = rows[-10:]

            rows.reverse()
        except :
            rows = []

        #rows.insert( 0, csvHeader )

        for row in rows :
            lat = row[10]
            lon = row[11]
            #if len( lat ) > 0 and lat[0] == '-' :
            #    lat = 'S' + lat[1:]
            #if len( lon ) > 0 and lon[0] == '-' :
            #    lon = 'W' + lon[1:]
            if lat != '' and lon != '' :
                 text += '<a href="/map/14/%s/%s">\n' %( lat, lon )
            if style == 'style="background-color:%s;"' % listColour0 : 
                style = 'style="background-color:%s;"' % listColour1
            else :
                style = 'style="background-color:%s;"' % listColour0
            # 0:chan 1:utime 2:isotime 3:source 4:heard 5:level 6:error 7:dti 8:name 9:symbol 10:latitude 11:longitude 12:speed 13:course 14:altitude 15:frequency 16:offset 17:tone 18:system 19:status 20:comment
            text += '<div class="row">\n'
            text += '<div class="col-xs-6 col-sm-2" %s>%s&nbsp;</div>\n' % ( style, row[3] )
            text += '<div class="col-xs-6 col-sm-4" %s>%s&nbsp;</div>\n' % ( style, row[5] )
            text += '<div class="col-xs-6 col-sm-2" %s>%s&nbsp;</div>\n' % ( style, row[9] )
            text += '<div class="col-xs-6 col-sm-4" %s>%s&nbsp;</div>\n' % ( style, row[2] )
            text += '</div>\n'
            text += '<div class="row">\n'
            text += '<div class="col-xs-6 col-sm-4" %s>%s&nbsp;</div>\n' % ( style, row[4] )
            text += '</div>\n'
            text += '<div class="row">\n'
            text += '<div class="col-xs-6 col-sm-2" %s>%s&nbsp;</div>\n' % ( style, lat )
            text += '<div class="col-xs-6 col-sm-2" %s>%s&nbsp;</div>\n' % ( style, lon )
            text += '<div class="col-xs-6 col-sm-2" %s>%s&nbsp;</div>\n' % ( style, row[12] )
            text += '<div class="col-xs-6 col-sm-1" %s>%s&nbsp;</div>\n' % ( style, row[13] )
            text += '<div class="col-xs-6 col-sm-1" %s>%s&nbsp;</div>\n' % ( style, row[14] )
            text += '<div class="col-xs-6 col-sm-4" %s>%s&nbsp;</div>\n' % ( style, row[18] )
            text += '</div>\n'
            for i in range( 19, len( row )) :
                if len( row[i] ) > 0 :
                    text += '<div class="row">\n'
                    text += '<div class="col-xs-12 col-sm-12" %s>%s&nbsp;</div>\n' % ( style, unicode( row[i], errors='replace') )
                    text += '</div>\n'
            if lat != '' and lon != '' :
                 text += '</a>\n'
            #for i in range( 0, len( row )) :
            #    text += '<div class="row">\n'
            #    text += '<div class="col-xs-12 col-sm-12" %s>%i %s&nbsp;</div>\n' % ( style, i, unicode( row[i], errors='replace'))
            #    text += '</div>\n'


        return render.base(header,text)

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
