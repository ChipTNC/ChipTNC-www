#!/usr/bin/python

import web
import time
from gps3 import gps3

render = web.template.render('templates/')

class map:
    '''
    https://github.com/heldersepu/GMapCatcher
    http://openlayers.org/
    var map = new ol.Map({
        target: "map",
        view: new ol.View({
            zoom: 1,
            center: [0, 0]
        }),
        layers: [
            new ol.layer.Tile({
                source: new ol.source.XYZ({
                    url: "Tiles/{x}/{y}/{z}.png",
                    maxZoom: 18
                })
            })
        ]
    });
    '''
    def GET(self, zoom =  "4", lat = "0", lon = "0" ):
        header = ' \n \
            <link rel="stylesheet" href="/static/css/ol.css" type="text/css"> \n \
            <style> \n \
                .map { \n \
                    height: 400px; \n \
                    width: 100%; \n \
                } \n \
            </style> \n \
            <script src="/static/js/ol.js" type="text/javascript"></script> \n \
            '
        markerJs = "var iconFeature = new ol.Feature({\n \
              geometry: new ol.geom.Point(%s),\n \
                name: '%s'\n \
                });\n \
                \n \
              var iconStyle = new ol.style.Style({\n \
                image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({\n \
                    anchor: [0.5, 46],\n \
                    anchorXUnits: 'fraction',\n \
                    anchorYUnits: 'pixels',\n \
                    opacity: 0.75,\n \
                    src: '/static/images/marker.png'\n \
                }))\n \
              });\n \
                \n \
              iconFeature.setStyle(iconStyle);\n \
                \n \
              var vectorSource = new ol.source.Vector({\n \
                features: [iconFeature]\n \
              });\n \
              \n \
              var vectorLayer = new ol.layer.Vector({\n \
                source: vectorSource\n \
              });\n \
        "
        html = ' \n \
            <h2>Map</h2> \n \
            <div id="map" class="map"></div> \n \
            <script type="text/javascript"> \n \
                %s \
                var map = new ol.Map({ \n \
                    target: "map", \n \
                    layers: [ \n \
                        new ol.layer.Tile({ \n \
                            source: new ol.source.OSM({ \n \
                                url: "/static/GMapCatcher/tiles/{z}/{x}/{y}.png", \n \
                                maxZoom: 17, \n \
                                minZoom: 0 \n \
                            }) \n \
                        }) \n \
                        %s \n \
                    ], \n \
                    view: new ol.View({ \n \
                        center: ol.proj.fromLonLat([%s, %s]), \n \
                        zoom: %s \n \
                    }) \n \
                }); \n \
            </script> \n \
        '
        text = ''
        if lon == '0' and lat == '0' and zoom == '4' :
            try :
                gps_socket = gps3.GPSDSocket()
                data_stream = gps3.DataStream()
                gps_socket.connect()
                gps_socket.watch()
                for i in range( 0,20 ) :
                    time.sleep( 0.1 )
                    new_data = gps_socket.next()
                    if new_data :
                        #text += "<div class='row'>" + str( data_stream.TPV['lat'] ) + ' ' + str( data_stream.TPV['lon'] ) + "</div>\n"
                        data_stream.unpack(new_data)
                        if data_stream.TPV['lat'] and data_stream.TPV['lat'] != 'n/a':
                            lat = str( data_stream.TPV['lat'] )
                        if data_stream.TPV['lon'] and data_stream.TPV['lon'] != 'n/a':
                            lon = str( data_stream.TPV['lon'] )
                    if lon != '0' and lat != '0' :
                        break
                gps_socket.close()
            except :
                pass

            marker = ''
            markerLayer = ''
        else :
            # fill in the namea
            location = "ol.proj.transform([%s, %s], 'EPSG:4326','EPSG:3857')" % ( lon, lat ) 
            marker = markerJs % ( location, 'somename' )
            markerLayer = ',vectorLayer'

        
        text += html % ( marker, markerLayer,  lon, lat, zoom )

        return render.base( header, text )

