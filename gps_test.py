#!/usr/bin/python

import time
from gps3 import gps3

gps_socket = gps3.GPSDSocket()
data_stream = gps3.DataStream()
gps_socket.connect()
gps_socket.watch()
#time.sleep( 1 )
i=0
while True:
    i += 1
    time.sleep( 0.1 )
    new_data = gps_socket.next()
    if new_data :
        data_stream.unpack(new_data)
    
    if data_stream.TPV['lat'] and data_stream.TPV['lat'] != 'n/a' and data_stream.TPV['lon'] and data_stream.TPV['lon'] != 'n/a':
        print( "Loop %d %s %s\n" % ( i, data_stream.TPV['lat'], data_stream.TPV['lon'] ))
        break
gps_socket.close()
