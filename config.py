import logging

logger = logging.getLogger( "ChipTNC" )

listColour0 = "White"
listColour1 = "WhiteSmoke"
#listColour0 = "lavender"
#listColour1 = "lavenderblush"

# misc utilities
def inputRender( formElement, class_ ) :
    labelClass = class_[0]
    if len( class_ ) > 1 : 
        divClass = class_[1]
    else :
        divClass = class_[0]
    text = '<div class="%s"><label for="%s">%s</label></div>\n' %( labelClass, formElement.id, formElement.description )
    text += '<div class="%s">%s</div>\n' %( divClass, formElement.render())
    return text 

# APRX configuration
def replaceConfigSection( linesIn, section, new ) :
    lines = []
    foundStart = False
    sectionEnd = "</%s" % ( section[1:] )
    logger.debug( "replaceConfigSection %s", section )
    for line in linesIn :
        logger.debug( "line %s", line )
        if line.find( section ) != -1 or foundStart :
            logger.debug( "found %s", line )
            foundStart = True
            if line.find( sectionEnd ) != -1 :
                foundStart = False
                outLines = new.split('\n')
                for outLine in outLines :
                    lines.append( outLine.strip( " " ) + '\n' ) 
        else : 
            lines.append( line ) 

    logger.debug( "outlines %s", lines )
    return lines

def getConfigSection( linesIn, section ) :
    lines = []
    foundStart = False
    sectionEnd = "</%s" % ( section[1:] )
    for line in linesIn :
        if line.find( section ) != -1 or foundStart :
            foundStart = True
            lines.append( line.strip( " " ) ) 
            if line.find( sectionEnd ) != -1 :
                foundStart = False

    return lines

# Direwolf configurations
def replaceConfigValue( linesIn, value, new ) :
    lines = []
    for line in linesIn :
        if line.startswith( value ) :
            lines.append( new ) 
        else : 
            lines.append( line ) 

    return lines

def deleteConfigValue( linesIn, value ) :
    lines = []
    for line in linesIn :
        if not line.startswith( value ) :
            lines.append( line ) 

    return lines

def commentConfigValue( linesIn, value ) :
    lines = []
    for line in linesIn :
        if line.startswith( value ) :
            lines.append( "#%s" % line ) 
        else :
            lines.append( line ) 

    return lines

def getConfigValues( values ) :
    retValues = {}
    conf = open( "/etc/direwolf.conf", 'rb')
    for line in conf :
        for value in values :
            if line.startswith( value ) :
                items = line.split( ' ', 1 )
                retValues[value] = items[1]

    return retValues

def configRead( filename ) :
    try :
        conf = open( filename, 'r')
        lines = conf.readlines()
        conf.close()
    except :
        return []

    return lines

def configSave( filename, lines ) :
    conf = open( filename, "w" )
    for line in lines :
        conf.write( line )
    conf.close()
