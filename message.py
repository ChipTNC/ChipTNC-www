#!/usr/bin/python

import os
import web
import aprs
import kiss
import sqlite3
import hashlib
import datetime, time
from web import form
from config import *

# should get this from /var/run somewhere
msgId = 0
dbPath = "/data/lib/aprs"
dbName = "messages.db"
dbFile = os.path.join( dbPath, dbName )

render = web.template.render('templates/')

messageForm = form.Form( 
        form.Textbox("Destination", 
            form.notnull,
        ),
        form.Textarea("Message", 
            form.notnull,
            size="64",
            maxlength="256"
        ),
        form.Checkbox("ACK_RX",
            description = "ACK RX",
            value = 'RX_ACK'
        ),
        form.Radio("SEND_AS", [ 'APRS RF', 'APRS IS', 'EMAIL' ],
            form.notnull,
            description = 'Send as',
        ),
    )

class message:
    def render(self, form) :
        text = ''
        text += '<h2>Send Message</h2>\n'
        text += '<div class="row">\n'
        text += inputRender( form['Destination'], ['col-xs-3 col-sm-2','col-xs-6 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['Message'], ['col-xs-3 col-sm-2','col-xs-6 col-sm-6'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['ACK_RX'], ['col-xs-3 col-sm-2', 'col-sm-6 col-xs-4'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['SEND_AS'], ['control-label col-xs-3 col-sm-2', 'col-xs-6 col-sm-6'] )
        text += '</div>\n'

        return text

    def GET(self):
        header = ''
        text = ''
        form = messageForm()
        
        #text += "<div class=\"row\"'%s'</div>" % ( " ".join( mixers ) )
        text += '<form name="message" method="post">\n'
        text += self.render( form )
        text += '<button type="submit">Send Message</button>\n</form>\n'

        return render.base(header,text)

    def POST(self):  
        global msgId
        form = messageForm() 
        text = ''
        if not form.validates(): 
            text = "<div class=\"error\">Invalid Message</div>\n"
        else:
            config = getConfigValues( [ "MYCALL", "IGLOGIN" ] ) 
            if form['ACK_RX'].checked :
                msgId += 1
                print("got ACK_RX")
            if form['SEND_AS'].value == "APRS RF" :
                frame = aprs.Frame()
                frame.source = aprs.Callsign(config['MYCALL'])
                frame.destination = aprs.Callsign('APDW13')
                frame.path = [aprs.Callsign('WIDE1-1'),aprs.Callsign('WIDE2-1')]
                frame.text = ':%-9s:' % str(form['Destination'].value)
                frame.text += str(form['Message'].value)
                frame.text += str("{%-5d" % msgId)
                ki = kiss.TCPKISS(host='localhost', port=8001)
                ki.start()
                ki.write(frame.encode_kiss())
                ki.stop()
                #text += "<div class=\"row\">Send RF %s</div>\n" % frame
            elif form['SEND_AS'].value == "APRS IS" and config['IGLOGIN'] :
                text = "<div class=\"row\">APRSIS send</div>\n"
            else:
                text = "<div class=\"row\">Can't send</div>\n"

            m = hashlib.md5()
            m.update( str( frame.source ))
            m.update( str( frame.destination ))
            m.update( frame.text )
            chksum = m.hexdigest()

            conn = sqlite3.connect( dbFile )
            cur = conn.cursor()
            sumVal = (chksum,)
            acked = False
            sent = True
            read = True
            now = datetime.datetime.now()
            values = ( now, now, str( frame.source ), str( frame.destination ), str( frame.path ), frame.text , chksum, msgId, acked, sent, read )
            cur.execute('INSERT INTO messages(RX_time, TX_time, source, destination, path, txt, chksum, msgId, acked, sent, read) VALUES (?,?,?,?,?,?,?,?,?,?,?)', values )
            conn.commit()
            conn.close()
            raise web.seeother('/messages')
         
        text += '<form name="message" method="post">\n'
        text += self.render( form )
        text += '<button type="submit">Send Message</button>\n</form>\n'

        return render.base("",text)

class messages:
    debug = False

    def GET(self):
        header = ''
        text = "<h2>Message List</h2>"
        try :
            conn = sqlite3.connect( dbFile )
            cur = conn.cursor()
        except :
	    return render.base(header,text)

        style = 'style="background-color:%s;"' % listColour0
        for row in cur.execute('SELECT source, destination, path, txt, msgId, acked, sent, id  FROM messages '):
            txtItems = row[3].split( ':' )
            if style.find(  'style="background-color:%s;"' % listColour0 ) >= 0 :
                style = 'style="background-color:%s;"' % listColour1
            else :
                style = 'style="background-color:%s;"' % listColour0

            text += "<div %s >" % style
            text += "<form name='message' method='post'>"
            text += "<input type='hidden' name='id' value='%s'/>" % row[7]
            text += "<div class='row'>"
            text += "<div class='col-xs-4'>From: %s</div>" % row[0]
            text += "<div class='col-xs-4'>To: %s</div>" % txtItems[1]
            if self.debug :
                text += "<div class='col-xs-4'>%s</div>" % row[1]
            text += "</div>"
            if self.debug :
                text += "<div class='row'>"
                line = row[2]
                if line[0] == '[' :
                    line = line[1:]
                if line[-1] == ']' :
                    line = line[:-1]
                for item in line.split( ',' ) :
                    text += "<div class='col-xs-4'>%s</div>" % item
                text += "</div>"

            if txtItems[2].find( "{" ) != -1 :
                msgItems = txtItems[2].split( '{' )
                msgTxt = msgItems[0]
                msgTxtId = msgItems[1]
            else :
                msgTxtId = None
                msgTxt = txtItems[2]

            text += "<div class='row'>"
            text += "<div class='col-xs-4'>%s</div>" % msgTxt
            text += "</div>"
            if self.debug :
                text += "<div class='row'>"
                if row[4] :
                    id = row[4]
                elif msgTxtId :
                    id = msgTxtId
                else :
                    id = 'None'
                text += "<div class='col-xs-4'>id '%s'</div>" % id
                text += "<div class='col-xs-4'>acked %s</div>" % row[5]
                text += "<div class='col-xs-4'>sent %s</div>" % row[6]
                text += "</div>"
            text += "<div class='row'>"
            text += "<div class='col-xs-4'>"
            text += '&nbsp;'
            text += "</div>"
            text += "<div class='col-xs-4'>"
            if row[6] == 0 :
                text += "<button type='submit' name='action' value='reply'>Reply</button>"
            elif row[6] > 4 :
                text += "<button type='submit' name='action' value='resend'>Re-send</button>"
            else :
                text += '&nbsp;'
            text += "</div>"
            text += "<div class='col-xs-2'>"
            text += "<button type='submit' name='action' value='delete'>Delete</button>"
            text += "</div>"
            if row[6] == 0 :
                # recieved
                statusStyle = "style='background-color:green;'"
            elif row[5] == 0 and row[6] > 4 :
                statusStyle = "style='background-color:red;'"
            elif row[5] > 0 and row[6] > 0 :
                statusStyle = "style='background-color:green;'"
            elif row[5] == 0 and row[6] > 0 :
                statusStyle = "style='background-color:yellow;'"
            text += "<div class='col-xs-1' %s>" % statusStyle
            text += '&nbsp;'
            #text += '<input class="form-control" type="text">'
            text += "</div>"
            text += "</div>"
            text += "</form>"
            text += "</div>"
        conn.close()
        return render.base(header,text)

    def POST(self):
        i = web.input()

        if i.action == 'delete' :
            if i.id :
                conn = sqlite3.connect( dbFile )
                cur = conn.cursor()
                cur.execute("DELETE FROM messages WHERE id=?", [ i.id, ] )
                conn.commit()
                conn.close()

        if i.action == 'resend' :
            if i.id :
                conn = sqlite3.connect( dbFile )
                cur = conn.cursor()
                cur.execute("UPDATE messages SET sent = 1 WHERE id=?", [ i.id, ] )
                conn.commit()
                conn.close()

        return self.GET()
