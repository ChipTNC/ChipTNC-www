#!/usr/bin/python

import aprs
import kiss
import logging
import datetime
import os
import sqlite3
import hashlib
import sqlite3
import datetime, time
import thread

def adapt_datetime(ts):
    return time.mktime(ts.timetuple())

sqlite3.register_adapter(datetime.datetime, adapt_datetime)

callsign = ''
ssid = ''
igLogin = ''
igPass = ''
msgId = 0
dbPath = "/data/lib/aprs"
dbName = "messages.db"
dbFile = os.path.join( dbPath, dbName )

if not os.path.exists(dbPath):
    os.makedirs(dbPath)

def parse_frame(frame):
    msgId = None
    try :
        aFrame = aprs.Frame(frame)
        #print(aFrame)
        if aFrame.text and aFrame.text[0] == ':':
            conn = sqlite3.connect( dbFile )
            cur = conn.cursor()
            items = aFrame.text.split( ':' )
            if len( items ) > 1 and items[1].startswith( callsign ) and items[2].startswith( 'ack' ) :
                msgId = items[2][3:]
                print( "Message ack '%s' %s" % ( msgId,  aFrame.text ))
                cur.execute('SELECT * FROM messages WHERE msgId=?', ( msgId, ))
                if cur.fetchone() :
                    print( "updating record" )
                    now = datetime.datetime.now()
                    numRows = cur.execute("UPDATE messages SET RX_time = ?,acked = ? WHERE msgId = ?", ( now, 1, msgId, ))
                    if numRows < 1 :
                        print( "update failed" )
                conn.commit()
                # clear the msgId so we don't ack this ack
                msgId = None
            elif len( items ) > 1 and items[1].startswith( callsign ) : 
                print( "Message frame " + aFrame.text )
                m = hashlib.md5()
                m.update( str( aFrame.source ))
                m.update( str( aFrame.destination ))
                m.update( aFrame.text )
                chksum = m.hexdigest()

                sumVal = (chksum,)
                cur.execute('SELECT * FROM messages WHERE chksum=?', sumVal)
                index = aFrame.text.rfind('{')
                if index != -1 :
                    msgId = aFrame.text[index+1:]
                if cur.fetchone() :
                    print( "Duplicate message" )
                    print(aFrame)
                else:
                    # message gets acked below
                    acked = True
                    sent = False
                    read = False
                    now = datetime.datetime.now()
                    values = ( now, now, str( aFrame.source ), str( aFrame.destination ), str( aFrame.path ), aFrame.text , chksum, msgId, acked, sent,read )
                    cur.execute('INSERT INTO messages(RX_time, TX_time, source, destination, path, txt, chksum, msgId, acked, sent, read) VALUES (?,?,?,?,?,?,?,?,?,?,?)', values )
                    conn.commit()
            else :
                print( "Message is not for us %s" % ( aFrame.text ))
            conn.close()
        else :
            print( "Not a message frame " + aFrame.text[0]  )
            print(aFrame)
            
    except Exception as inst :
        print( "Frame parsing failed %s %s" % ( type(inst), inst.args ))

    if msgId :
        ki = kiss.TCPKISS(host='localhost', port=8001 )
        aFrame.text = ':%-9s:ack%s' % ( aFrame.source, msgId )
        aFrame.path = [aprs.Callsign('WIDE1-1'),aprs.Callsign('WIDE2-1')]
        aFrame.source = aprs.Callsign( "%s-%s" % ( callsign, ssid ))
        #aFrame.source = aprs.Callsign( "APDW13" )
        aFrame.destination = aprs.Callsign('APDW13')
        print( "ACKing message " + aFrame.text  )
        ki.start()
        ki.write(aFrame.encode_kiss())
        ki.stop()

def init_messages() :
    global callsign
    global ssid
    global igLogin
    global igPass

    conf = open( "/etc/direwolf.conf", 'rb')
    for line in conf :
        if line.startswith( "MYCALL" ) :
            words = line.split()
            items = words[1].split( '-' )
            callsign = items[0]
            ssid = items[1]
            print( "Found call sign %s ssid %s" % ( callsign, ssid ))
        if line.startswith( "IGLOGIN" ) :
            words = line.split()
            print( "words %s" % words )
            #items = words[1].split( ' ' )
            igLogin = words[1].strip()
            igPass = words[2].strip()

    conf.close()

def check_db() :
    if not os.path.exists( dbFile ) :
        conn = sqlite3.connect( dbFile )
        cur = conn.cursor()
        cur.execute('''CREATE TABLE messages
                        ( id INTEGER PRIMARY KEY, RX_time int, TX_time int, source text, destination text, path text, txt text, chksum text, msgId text, acked int, sent int, read int )''')
        conn.commit()
        conn.close()
        os.chmod( dbFile, 0664 ) 
        os.chown( dbFile, -1, 33 )

def worker( args ) :
    while True :
        time.sleep(60)
        #try :
        if True :
            conn = sqlite3.connect( dbFile )
            cur = conn.cursor()
            cur.execute('SELECT id, source, txt, sent FROM messages WHERE acked = 0 and sent > 0 and sent < 5' )
            for row in cur.fetchall() :
                print( "row %d %s %s %d" % ( row[0], row[1], row[2], row[3] ))
                aFrame = aprs.Frame()
                #aFrame.text = ':%-9s:%s' % ( row[1], row[2])
                aFrame.text = ':%s' % ( row[2] )
                aFrame.path = [aprs.Callsign('WIDE1-1'),aprs.Callsign('WIDE2-1')]
                aFrame.source = aprs.Callsign( "%s-%s" % ( callsign, ssid ))
                #aFrame.source = aprs.Callsign( "APDW13" )
                aFrame.destination = aprs.Callsign('APDW13')
                print( "Re-sending message " + row[2]  )
                ki = kiss.TCPKISS(host='localhost', port=8001 )
                if ki :
                    ki.start()
                    ki.write(aFrame.encode_kiss())
                    ki.stop()
                
                    print( "worker updating record" )
                    now = datetime.datetime.now()
                    numRows = cur.execute("UPDATE messages SET TX_time = ?,sent = ? WHERE id = ?", ( now, int( row[3] ) + 1, row[0], ))
                    if numRows < 1 :
                        print( "update failed" )
                    conn.commit()
                else :
                    print( "KISS conect failed " )

            conn.close()
        #except :
        #    print( "worker thread ack test failed" )
            
                

def main():
    _logger = logging.getLogger( 'aprs.classes' )
    _logger.setLevel( logging.INFO )
    _logger = logging.getLogger( 'kiss.classes' )
    _logger.setLevel( logging.INFO )

    print( "Getting configuration" );
    init_messages()
    print( "Checking database" )
    check_db()

    thread.start_new_thread ( worker, ( None, ) )

    ki = kiss.TCPKISS(host='localhost', port=8001 )
    ki.start()
    ki.read(callback=parse_frame)

    print( "leaving main" )
    
if __name__ == '__main__':
    main()
