#!/usr/bin/python

import urllib2
import os

hostname = os.uname()[1]

if -1 == hostname.find( "pro" ) :
    print( "Running on a standard C.H.I.P." )
    f = open( "/var/log/lighttpd/access.log", "r" )
else :
    print( "Running on a C.H.I.P. Pro" )
    f = open( "/var/log/lighttpd_access.log", "r" )


lines = f.readlines()
for line in lines :
    #if line.find( "404" ) and line.find( "GMapCatcher" ) :
    items = line.split( " " )
    if items[8] == '404' and items[6].find( "GMapCatcher" ) and items[6].startswith( "/static/GMapCatcher/tiles" ):
        count = 1 
        print( "%s %s" % ( items[6], items[10] ))
        urlParts = items[6].split( '/' )
        print ( "parts " + ' '.join( urlParts ))
        z = int( urlParts[4] )
        x = int( urlParts[5] )
        y = int( urlParts[6][:-4] )
        filePath = "/var/www/html/static/GMapCatcher/tiles/%d/%d/%d.png" % (z, x, y )
        if not os.path.isfile( filePath ) :
            mapUrl = 'https://%i.base.maps.cit.api.here.com/maptile/2.1/maptile/newest/normal.day/%i/%i/%i/256/png8' + \
                    '?app_id=gzBYPog1tMKHNeAtWr3x' + \
                    '&app_code=A4B1OVbsqrYMG8Dw0_FgjA'
            mapUrl = mapUrl % ( count, z, x, y )
            print mapUrl
            response = urllib2.urlopen( mapUrl )
            html = response.read()
            d = "/var/www/html/static/GMapCatcher/tiles/%d/" % (z )
            if not os.path.exists(d):
                os.makedirs(d)
            d = "/var/www/html/static/GMapCatcher/tiles/%d/%d" % (z,x )
            if not os.path.exists(d):
                os.makedirs(d)
            outputFile = open( filePath, "wb" )
            outputFile.write( html )
            outputFile.close()
        #i = 0
        #for item in items :
        #    print( "%d:%s" % ( i, item ))
        #    i += 1

f.close()
