#!/usr/bin/python

import os
import web
import alsaaudio
import subprocess
from web import form
from config import *

render = web.template.render('templates/')

REV=3

GPIO = {}
if REV == 1 :
    GPIO['RF PTT'] = 412
    GPIO['M PTT'] = 412
    GPIO['S PTT'] = 412
    GPIO['POWER LEVEL'] = 412
    GPIO['RF ENABLE'] = 412
    GPIO['RF POWER ENABLE'] = 412
elif REV == 2 :
    GPIO['RF PTT'] = 106
    GPIO['M PTT'] = 103
    GPIO['S PTT'] = 102
    GPIO['POWER LEVEL'] = 412
    GPIO['RF ENABLE'] = 412
    GPIO['RF POWER ENABLE'] = 412
else : 
    GPIO['RF PTT'] = 39
    GPIO['Multiplex PTT'] = 40
    GPIO['Simplex PTT'] = 41
    GPIO['Power level'] = 38
    GPIO['RF enable'] = 37
    GPIO['RF POWER enable'] = 100

settingsForm = form.Form(
        form.Textbox("MYCALL",
            form.notnull,
            description = 'Call sign',
            class_="form-control",
        ),
        form.Textbox("PASSWD",
            description = 'APRS-IS Password',
            class_="form-control",
        ),
        form.Radio("IG_ENABLE", [ 'Yes', 'No' ],
            form.notnull,
            description = 'IGATE Enable',
            class_="radio-inline",
        ),
        form.Radio("DIGI_ENABLE", [ 'Yes', 'No' ],
            form.notnull,
            description = 'DIGIPEAT Enable',
            class_="radio-inline",
        ),
        form.Textbox( "VISCOUS",
            form.regexp('\d+', 'Must be a digit'),
            form.Validator('Must be more than or equal to 0', lambda x:int(x)>=0),
            form.Validator('Must be less than 10', lambda x:int(x)<10),
            description="Viscous Delay", 
            class_="form-control",
        ),
        form.Textbox("FILTERS",
            description = 'Digi Filters',
            class_="form-control",
        ),
        form.Radio("TB_ENABLE", [ 'Yes', 'No' ],
            form.notnull,
            description = 'TIME BEACONING',
        ),
        form.Textbox("TB_TIME", 
            form.notnull,
            description = "BEACON TIME",
            class_="form-control",
        ),
        form.Textbox("TB_PATH", 
            form.notnull,
            description = "PATH",
            class_="form-control",
        ),
        form.Textbox("TB_SYMBOL", 
            form.notnull,
            description = "SYMBOL",
            class_="form-control",
        ),
        form.Textbox("TB_ALT", 
            form.notnull,
            description = "ALTITUDE",
            class_="form-control",
        ),
        form.Textbox("TB_DELAY", 
            form.notnull,
            description = "START DELAY",
            class_="form-control",
        ),
        form.Textbox("TB_COMMENT", 
            form.notnull,
            description = "COMMENT",
            class_="form-control",
        ),
        form.Radio("SB_ENABLE", [ 'Yes', 'No' ],
            form.notnull,
            description = 'SMART BEACONING',
        ),
        form.Textbox("SB_HIGH_SPEED", 
            form.notnull,
            description = 'HIGH SPEED',
            class_="form-control",
        ),
        form.Textbox("SB_HIGH_TIME", 
            form.notnull,
            description = 'HIGH TIME',
            class_="form-control",
        ),
        form.Textbox("SB_LOW_SPEED", 
            form.notnull,
            description = 'LOW SPEED',
            class_="form-control",
        ),
        form.Textbox("SB_LOW_TIME", 
            form.notnull,
            description = 'LOW TIME',
            class_="form-control",
        ),
        form.Textbox("SB_MIN_DELAY", 
            form.notnull,
            description = 'MIN DELAY',
            class_="form-control",
        ),
        form.Textbox("SB_TURN_ANGLE", 
            form.notnull,
            description = 'TURN ANGLE',
            class_="form-control",
        ),
        form.Textbox("SB_TURN_SLOPE", 
            form.notnull,
            description = 'TURN SLOPE',
            class_="form-control",
        ),
        form.Textbox("Volume", 
            form.notnull,
            form.regexp('\d+', 'Must be a digit'),
            form.Validator('Must be more than 0', lambda x:int(x)>0),
            form.Validator('Must be less than or equal to 100', lambda x:int(x)<=100),
            class_="form-control",
        ),
        form.Dropdown('PTT', 
            [('MULTIPLEX', 'Multiplexed'), ('PTT', 'Dedicated'), ( "RFMODULE", "RF Module" ) ]
        ),

    )
    
gpioForm = form.Form(
        form.Dropdown('RF PTT dir', 
            [('in', 'In'), ('out', 'Out') ]
        ),
        form.Dropdown('RF PTT val', 
            [('0', 'Low'), ('1', 'High') ]
        ),
        form.Dropdown('Simplex PTT dir', 
            [('in', 'In'), ('out', 'Out') ]
        ),
        form.Dropdown('Simplex PTT val', 
            [('0', 'Low'), ('1', 'High') ]
        ),
        form.Dropdown('Multiplex PTT dir', 
            [('in', 'In'), ('out', 'Out') ]
        ),
        form.Dropdown('Multiplex PTT val', 
            [('0', 'Low'), ('1', 'High') ]
        ),
        form.Dropdown('Power level dir', 
            [('in', 'In'), ('out', 'Out') ]
        ),
        form.Dropdown('Power level val', 
            [('0', 'Low'), ('1', 'High') ]
        ),
        form.Dropdown('RF enable dir', 
            [('in', 'In'), ('out', 'Out') ]
        ),
        form.Dropdown('RF enable val', 
            [('0', 'Low'), ('1', 'High') ]
        ),
        form.Dropdown('RF POWER enable dir', 
            [('in', 'In'), ('out', 'Out') ]
        ),
        form.Dropdown('RF POWER enable val', 
            [('0', 'Low'), ('1', 'High') ]
        ),
    )

class gpios:

    def render(self, form) :
        text = ''
        
        for key in GPIO :
            text += '<h3>'+key+'</h3>\n'
            text += '<div class="row">\n' 
            text += inputRender( form[key+' dir'], ['col-xs-5 col-sm-3 ','col-xs-5 col-sm-2'] )
            text += inputRender( form[key+' val'], ['col-xs-5 col-sm-3 ','col-xs-5 col-sm-2'] )
            text += '</div>\n'

        return text 

    def setGPIO(self,gpio, val, direction) :
        try :
            logger.debug( "setting : /sys/class/gpio/gpio%d/value %s" % ( gpio, val ))
            f = open( "/sys/class/gpio/gpio%d/value" % gpio )
            f.write( val.strip() + "\n" )
            f.close()
        except :
            logger.debug( "setting val %s failed" % val)
        try :
            logger.debug( "setting : /sys/class/gpio/gpio%d/directioni %s" % ( gpio, direction ))
            f = open( "/sys/class/gpio/gpio%d/direction" % gpio )
            f.write( direction.strip() + "\n" )
            f.close()
        except :
            logger.debug( "setting direction %s failed" % ( direction ))


    def readGPIO(self,gpio) :
        try :
            logger.debug( "reading : /sys/class/gpio/gpio%d/direction" % gpio )
            f = open( "/sys/class/gpio/gpio%d/direction" % gpio )
            direction = f.read().strip()
            f.close()
            logger.debug( "reading : /sys/class/gpio/gpio%d/value" % gpio )
            f = open( "/sys/class/gpio/gpio%d/value" % gpio )
            val = f.read().strip()
            f.close()
        except :
            return "0", "in"

        logger.debug( "returning %s %s" % ( val, direction ))

        return val, direction

    def GET(self) :
        header = ''
        form = gpioForm()

        for key in GPIO :
            val, direction = self.readGPIO( GPIO[key] ) 
            form[key+' dir'].value = direction
            form[key+' val'].value = val
            #logger.debug( "set %s %s %s" % ( key, val, direction ))

        text = ''
        text += '<form name="main" method="post">\n'
        text += self.render(form)
        #text += form.render()
        text += '<button type="submit">Set Settings</button>\n</form>\n'

        #print( "Header %s" % header )
        #print( "Text %s" % text )

        return render.base(header,text)

    def POST(self): 
        form = gpioForm() 
        text = ''
        header = ''
        text += '<form name="main" method="post">\n'
        if not form.validates(): 
            text = "<div class=\"error\">Invalid Setting</div>\n"
        else:
            for key in GPIO :
                val, direction = self.readGPIO( GPIO[key] ) 
                if ( form[key+' val'].value != val ) or (form[key+' dir'].value != direction ):
                    self.setGPIO( GPIO[key], form[key+' val'].value, form[key+' dir'].value )
        
        text += self.render(form)
        text += '<button type="submit">Set Settings</button>\n</form>\n'

        #print( "Header %s" % header )
        #print( "Text %s" % text )

        return render.base(header,text)
class settings:
    DWfile = "/etc/direwolf.conf" 
    APRXfile = "/etc/aprx.conf" 
   
    def render(self, form) :
        text = ''
        text += '<h2>APRS configuration</h2>\n'
        text += '<div class="row">\n'
        text += inputRender( form['MYCALL'], ['col-xs-4 col-sm-2','col-xs-6 col-sm-2'] )
        text += inputRender( form['PASSWD'], ['col-xs-4 col-sm-2','col-xs-6 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['IG_ENABLE'], ['col-xs-4 col-sm-2', 'col-sm-2 col-xs-4'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['DIGI_ENABLE'], ['control-label col-xs-4 col-sm-2', 'col-xs-6 col-sm-2'] )
        text += '<div class="hidden-sm col-xs-12">  </div>\n'
        text += inputRender( form['VISCOUS'], ['control-label col-xs-4 col-sm-2 ','col-xs-6 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n'
        text += inputRender( form['FILTERS'], ['col-xs-4 col-sm-2','col-xs-8 col-sm-6'] )
        text += '</div>\n'
        text += '<h2>Time Beacon</h2>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['TB_ENABLE'], ['col-sm-2 col-xs-4'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['TB_TIME'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += inputRender( form['TB_DELAY'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['TB_SYMBOL'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += inputRender( form['TB_ALT'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['TB_PATH'], ['col-xs-2','col-xs-10 col-sm-6'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['TB_COMMENT'], ['col-xs-2','col-xs-10 col-sm-6'] )
        text += '</div>\n'
        text += '<h2>Smart Beaconing</h2>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['SB_ENABLE'], ['col-xs-4 col-sm-2','col-xs-4 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['SB_HIGH_SPEED'], ['col-xs-2', 'col-xs-4 col-sm-2'] )
        text += inputRender( form['SB_HIGH_TIME'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['SB_LOW_SPEED'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += inputRender( form['SB_LOW_TIME'], ['col-xs-2','col-xs-4 col-sm-2'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['SB_MIN_DELAY'], ['col-xs-2','col-sm-2 col-xs-4'] )
        text += inputRender( form['SB_TURN_ANGLE'], ['col-xs-2','col-sm-2 col-xs-4'] )
        text += '</div>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['SB_TURN_SLOPE'], ['col-xs-2','col-sm-2 col-xs-4'] )
        text += '</div>\n'
        text += '<h2>Audio</h2>\n'
        text += '<div class="row">\n' 
        text += inputRender( form['Volume'], ['col-xs-2 col-sm-2 ','col-xs-4 col-sm-2'] )
        text += inputRender( form['PTT'], ['col-xs-2 col-sm-2 ','col-xs-4 col-sm-2'] )
        text += '</div>\n'

        return text

    def GET(self):
        form = settingsForm()
        header = ''
        text = ''
        rows = []
        style = ''

        form['SB_ENABLE'].value = 'No'
        form['TB_ENABLE'].value = 'No'
        form['DIGI_ENABLE'].value = 'No'
        form['IG_ENABLE'].value = 'No'
        form['VISCOUS'].value = 0
        form['PTT'].value = 'MULTIPLEX'

        lines = configRead( self.DWfile )
        text += '<form name="main" method="post">\n'
        for line in lines :
            if line.startswith( "TBEACON" ) :
                form['TB_ENABLE'].value = 'Yes'
                words = line.split()
                inComment = False
                comment = ''
                for word in words:
                    if inComment :
                        comment += word + ' '
                        if word[-1] == quote :
                            comment = comment[:-2]
                            inComment = False
                            form['TB_COMMENT'].value = comment
                    items = word.split( "=" )
                    if items[0] == 'COMMENT' :
                        comment += items[1][1:] + ' '
                        quote = items[1][0]
                        if comment[-1] == quote :
                            comment = comment[:-1]
                        else :
                            inComment = True
                    if items[0] == 'EVERY' :
                        form['TB_TIME'].value = items[1]
                    if items[0] == 'VIA' :
                        form['TB_PATH'].value = items[1]
                    if items[0] == 'SYMBOL' :
                        form['TB_SYMBOL'].value = items[1]
                    if items[0] == 'ALTITUDE' :
                        form['TB_ALT'].value = items[1]
                    if items[0] == 'DELAY' :
                        form['TB_DELAY'].value = items[1]

            if line.startswith( "SMARTBEACONING" ) :
                form['SB_ENABLE'].value = 'Yes'
                words = line.split()
                form['SB_HIGH_SPEED'].value = words[1]
                form['SB_HIGH_TIME'].value = words[2]
                form['SB_LOW_SPEED'].value = words[3]
                form['SB_LOW_TIME'].value = words[4]
                form['SB_MIN_DELAY'].value = words[5]
                form['SB_TURN_ANGLE'].value = words[6]
                form['SB_TURN_SLOPE'].value = words[7]
            if line.startswith( "MYCALL" ):
                form['MYCALL'].value = line[len( "MYCALL" ) + 1:]
            if line.startswith( "IGLOGIN" ) :
                words = line.split()
                form['PASSWD'].value = words[2]
                form['IGATE_ENABLE'].value = 'Yes'
            if line.startswith( "PTT GPIO" ) :
                words = line.split()
                if REV == 1 :
                    if words[2] == '412' :
                        form['PTT'].value = 'PTT'
                    if words[2] == '413' :
                        form['PTT'].value = 'MULTIPLEX'
                    if words[2] == '-412' :
                        form['PTT'].value = 'RF_MODULE'
                else :
                    if words[2] == '102' :
                        form['PTT'].value = 'PTT'
                    if words[2] == '103' :
                        form['PTT'].value = 'MULTIPLEX'
                    if words[2] == '-106' :
                        form['PTT'].value = 'RF_MODULE'


        try :
            kwargs = {}
            mixer = alsaaudio.Mixer( 'Power Amplifier', **kwargs)
           
            volumes = mixer.getvolume()
            #for i in range(len(volumes)):
            #    text += "<div class=\"row\">Channel %i volume: %i%%</div>" % (i,volumes[i])
            form['Volume'].value = volumes[0]
        except :
            form['Volume'].value = 50

        form['FILTERS'].value = '' 

        lines = configRead( self.APRXfile )
        section = getConfigSection( lines, "<interface>" )
        for line in section :
            if line.find( "tx-ok" ) != -1 :
                items = line.strip().split()
                if len( items ) > 1 and items[1] == 'true' :
                    form['DIGI_ENABLE'].value = 'Yes'
        for line in lines :
            if line.strip().startswith( "#" ) :
                continue

            if line.find( "filter" ) != -1 : 
                #text += "<div>line %s</div>" % line
                items = line.strip().split( " " )
                if len( items ) > 1 :
                    if form['MYCALL'].value and line.find( '-b/%s' % form['MYCALL'].value ) >= 0 :
                        # filter out the blacklist rule for us
                        pass
                    else :
                        form['FILTERS'].value += '%s ' % items[1]
            if line.find( "viscous-delay" ) != -1 :
                #text += "<div>line %s</div>" % line
                items = line.strip().split( " " )
                try :
                    form['VISCOUS'].value = int( items[1] )
                except :
                    text += "<div>%s</div>" % line

        #text += form.render()
        text += self.render(form)
        text += '<button type="submit">Set Settings</button>\n</form>\n'
        
        return render.base(header,text)

    def POST(self): 
        form = settingsForm() 
        text = ''
        header = ''
        text += '<form name="main" method="post">\n'
        if not form.validates(): 
            text = "<div class=\"error\">Invalid Setting</div>\n"
            text += form.render()
        else:
            lines = configRead( self.DWfile )
            #lines = [x.strip() for x in lines] 
            #text = "<div class=\"row\">Form Correct</div>\n"
            # hide old settings
            line = "MYCALL %s\n" % form['MYCALL'].value
            lines = replaceConfigValue( lines, "MYCALL", line )
            if form['SB_ENABLE'].value == 'Yes' or form['TB_ENABLE'].value == 'Yes' :
                line = "TBEACON"
                if form['TB_TIME'].value != '' :
                    line += " EVERY=%s" % form['TB_TIME'].value
                if form['TB_PATH'].value != '' :
                    line += " VIA=%s" % form['TB_PATH'].value
                if form['TB_SYMBOL'].value != '' :
                    line += " SYMBOL=%s" % form['TB_SYMBOL'].value
                if form['TB_ALT'].value != '' :
                    line += " ALTITUDE=%s" % form['TB_ALT'].value
                if form['TB_DELAY'].value != '' :
                    line += " DELAY=%s" % form['TB_DELAY'].value
                if form['TB_COMMENT'].value != '' :
                    line += " COMMENT=\"%s\"" % form['TB_COMMENT'].value

                line += "\n"
                lines = replaceConfigValue( lines, "TBEACON", line )

            if form['SB_ENABLE'].value == 'Yes' :
                line = "SMARTBEACONING %s %s %s %s %s %s %s" % (
                        form['SB_HIGH_SPEED'].value,
                        form['SB_HIGH_TIME'].value,
                        form['SB_LOW_SPEED'].value,
                        form['SB_LOW_TIME'].value,
                        form['SB_MIN_DELAY'].value,
                        form['SB_TURN_ANGLE'].value,
                        form['SB_TURN_SLOPE'].value,
                    )

                line += "\n"
                lines = replaceConfigValue( lines, "SMARTBEACONING", line )

            if form['IG_ENABLE'].value == 'Yes' and form['PASSWD'].value != '' :
                line += "IGLOGIN %s %s\n" %( form['MYCALL'].value, form['PASSWD'].value )
                lines = replaceConfigValue( lines, "IGLOGIN", line )

            if REV == 1 :
                if form['PTT'].value == 'MULTIPLEX' :
                    lines = replaceConfigValue( lines, "PTT", "PTT GPIO 413\n" )
                if form['PTT'].value == 'PTT' :
                    lines = replaceConfigValue( lines, "PTT", "PTT GPIO 412\n" )
                if form['PTT'].value == 'RFMODULE' :
                    lines = replaceConfigValue( lines, "PTT", "PTT GPIO -412\n" )
            else :
                if form['PTT'].value == 'MULTIPLEX' :
                    lines = replaceConfigValue( lines, "PTT", "PTT GPIO 103\n" )
                if form['PTT'].value == 'PTT' :
                    lines = replaceConfigValue( lines, "PTT", "PTT GPIO 102\n" )
                if form['PTT'].value == 'RFMODULE' :
                    lines = replaceConfigValue( lines, "PTT", "PTT GPIO -106\n" )

            configSave( self.DWfile, lines )
            
            try :   
                kwargs = {}
                channel = alsaaudio.MIXER_CHANNEL_ALL
                mixer = alsaaudio.Mixer( 'Power Amplifier', **kwargs)
                volume = int( form['Volume'].value )
                mixer.setvolume(volume, channel)
            except :
                pass
            
            lines = configRead( self.APRXfile )
            line = "mycall %s\n" % form['MYCALL'].value
            lines = replaceConfigValue( lines, "mycall", line )
            newIface = "<interface>\n \
                   \ttcp-device 127.0.0.1 8001 KISS\n \
                   \ttx-ok %s\n \
                   </interface>" 
            newDigi = "<digipeater>\n \
                    \ttransmitter $mycall\n \
                    \t<source>\n \
                    \t\tviscous-delay %s\n \
                    \t\tsource $mycall\n \
                    %s \
                    \t</source>\n \
                    </digipeater>"
            if form['DIGI_ENABLE'].value == 'Yes' :
                lines = replaceConfigSection( lines, "<interface>", newIface % "true" )
                filters = ''
                for f in form['FILTERS'].value.strip().split( ' ' ) :
                    filters += "\t\tfilter %s\n" % ( f )
                # don't digipeat ourselves
                filters += '\t\tfilter -b/%s\n' % form['MYCALL'].value
                newDigi = newDigi %( str( form['VISCOUS'].value ), filters ) 
                lines = replaceConfigSection( lines, "<digipeater>", newDigi )
            else :
                lines = replaceConfigSection( lines, "<interface>", newIface % "false" )
            
            configSave( self.APRXfile, lines )

            #os.system( "/usr/local/bin/restart_aprs.sh" )
            #out = subprocess.check_output(["/usr/local/bin/restart_aprs.sh"], shell=True )
            #logger.debug( "restart %s" % out )
            try :
                subprocess.check_output(["/usr/bin/supervisorctl", "restart", "direwolf" ], shell=False )
            except :
                logger.debug( "DireWolf restart failed" )
            try :
                subprocess.check_output(["/usr/bin/supervisorctl", "restart", "aprx" ], shell=False )
            except :
                logger.debug( "APRX restart failed" )


        text += self.render(form)
        #text += form.render()
        text += '<button type="submit">Set Settings</button>\n</form>\n'

        return render.base(header,text)

class audio:
    def GET(self):
        kwargs = {}
        header = ''
        text = ''
        form = volumeForm()
        #mixers = alsaaudio.mixers()
        #for m in alsaaudio.mixers():
        #    text += "<div class=\"row\">'%s'</div>" % m
        
        mixer = alsaaudio.Mixer( 'Power Amplifier', **kwargs)
       
        volumes = mixer.getvolume()
        for i in range(len(volumes)):
            text += "<div class=\"row\">Channel %i volume: %i%%</div>" % (i,volumes[i])
        #text += "<div class=\"row\"'%s'</div>" % ( " ".join( mixers ) )
        form['Volume'].value = volumes[0]
        text += '<form name="main" method="post">\n'
        text += form.render()
        text += '<button type="submit">Set Volume</button>\n</form>\n'

        return render.base(header,text)

    def POST(self): 
        form = volumeForm() 
        text = ''
        if not form.validates(): 
            text = "<div class=\"error\">Invalid Volume</div>\n"
        else:
            kwargs = {}
            channel = alsaaudio.MIXER_CHANNEL_ALL
            mixer = alsaaudio.Mixer( 'Power Amplifier', **kwargs)
            volume = int( form['Volume'].value )
            mixer.setvolume(volume, channel)
            # form.d.boe and form['boe'].value are equivalent ways of
            # extracting the validated arguments from the form.
            # text = "<div class=\"row\">%s</div>\n" % (volume)
         
        text += '<form name="main" method="post">\n'
        text += form.render()
        text += '<button type="submit">Set Volume</button>\n</form>\n'

        return render.base("",text)

